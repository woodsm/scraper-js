<!-- @format -->

# scraper.js

## Summary

A node project to save all .html content from a web page once its idle

## Usage

Set the 'host' variable to desired site in .env

> npm run start ... // will run ~/scraper.ts
