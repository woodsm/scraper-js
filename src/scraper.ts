/**
 * Scraper.js
 *
 * node.js web crawler to save all .html content from a webpage to a local file
 *
 * @format
 */
require('dotenv').config()
// web address to scrape()
const address = process.env.HOST
// npm lib for headless chrome
const puppeteer = require('puppeteer')
// node file system
const fs = require('fs')
// npm lib for logging
const chalk = require('chalk')
// log coloring
const error = chalk.bold.red
const warning = chalk.green
const plain = chalk.bold.white
/**
 * scrape()
 *
 * Visit a web page save all .html to a file named by the webpages title.
 *
 * Config:
 * address = 'https://the webpage you wish to save'
 *
 * Usage:
 * > npm run start
 *
 * or
 *
 * > node scraper.min.js
 *
 */
async function scrape() {
 try {
  console.log(plain('Starting - Scraper.js'))
  // start new browser
  const browser = await puppeteer.launch()
  // open new page
  const page = await browser.newPage()
  console.log(warning(`Visiting: ... ${address}`))
  // goto $address and then wait until Idle
  await page.goto(address, { waitUntil: 'networkidle2' })
  // pull title and save as $title
  const title = await page.title()
  console.log(warning(`Scraping: ... ${address}`))
  // pull all .html content and save as $r
  const html = await page.content()
  // name extract file from <title> content
  const extract = `${title}.export.html`

  // if try succeeds write .html content to file and then leave
  return (
   console.log(warning(`Saving:   ... /${extract}`)),
   // write to a file named by the .html content title
   fs.writeFileSync(extract, html),
   // wait for browser to close
   await browser.close(),
   console.log(plain(`Success`)),
   // bye
   process.exit(0)
  )
  // if error log it and then leave
 } catch (e) {
  // exit out and log error from scrape()
  return console.log(error(`Failure  - Scraper.js ${e}`)), process.exit(1)
 }
}
scrape()
